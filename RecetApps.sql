PGDMP     #                    y            RecetAppsV01    13.3    13.3     �           0    0    ENCODING    ENCODING        SET client_encoding = 'UTF8';
                      false            �           0    0 
   STDSTRINGS 
   STDSTRINGS     (   SET standard_conforming_strings = 'on';
                      false            �           0    0 
   SEARCHPATH 
   SEARCHPATH     8   SELECT pg_catalog.set_config('search_path', '', false);
                      false            �           1262    16397    RecetAppsV01    DATABASE     j   CREATE DATABASE "RecetAppsV01" WITH TEMPLATE = template0 ENCODING = 'UTF8' LOCALE = 'Spanish_Chile.1252';
    DROP DATABASE "RecetAppsV01";
                postgres    false            �            1259    16462    receta    TABLE     �   CREATE TABLE public.receta (
    id integer NOT NULL,
    username character varying(255),
    titulo character varying(255),
    ingredientes character varying(1000),
    preparacion character varying(1000),
    "updatedAt" date,
    "createdAt" date
);
    DROP TABLE public.receta;
       public         heap    postgres    false            �            1259    16460    receta_id_seq    SEQUENCE     �   CREATE SEQUENCE public.receta_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 $   DROP SEQUENCE public.receta_id_seq;
       public          postgres    false    203            �           0    0    receta_id_seq    SEQUENCE OWNED BY     ?   ALTER SEQUENCE public.receta_id_seq OWNED BY public.receta.id;
          public          postgres    false    202            �            1259    16449    users    TABLE     �   CREATE TABLE public.users (
    id integer NOT NULL,
    username character varying(255) NOT NULL,
    "contraseña" character varying(255),
    email character varying(255),
    "updatedAt" date,
    "createdAt" date
);
    DROP TABLE public.users;
       public         heap    postgres    false            �            1259    16447    users_id_seq    SEQUENCE     �   CREATE SEQUENCE public.users_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 #   DROP SEQUENCE public.users_id_seq;
       public          postgres    false    201            �           0    0    users_id_seq    SEQUENCE OWNED BY     =   ALTER SEQUENCE public.users_id_seq OWNED BY public.users.id;
          public          postgres    false    200            +           2604    16465 	   receta id    DEFAULT     f   ALTER TABLE ONLY public.receta ALTER COLUMN id SET DEFAULT nextval('public.receta_id_seq'::regclass);
 8   ALTER TABLE public.receta ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    203    202    203            *           2604    16452    users id    DEFAULT     d   ALTER TABLE ONLY public.users ALTER COLUMN id SET DEFAULT nextval('public.users_id_seq'::regclass);
 7   ALTER TABLE public.users ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    201    200    201            �          0    16462    receta 
   TABLE DATA           k   COPY public.receta (id, username, titulo, ingredientes, preparacion, "updatedAt", "createdAt") FROM stdin;
    public          postgres    false    203          �          0    16449    users 
   TABLE DATA           ]   COPY public.users (id, username, "contraseña", email, "updatedAt", "createdAt") FROM stdin;
    public          postgres    false    201   �       �           0    0    receta_id_seq    SEQUENCE SET     <   SELECT pg_catalog.setval('public.receta_id_seq', 33, true);
          public          postgres    false    202            �           0    0    users_id_seq    SEQUENCE SET     ;   SELECT pg_catalog.setval('public.users_id_seq', 11, true);
          public          postgres    false    200            1           2606    16470    receta receta_pkey 
   CONSTRAINT     P   ALTER TABLE ONLY public.receta
    ADD CONSTRAINT receta_pkey PRIMARY KEY (id);
 <   ALTER TABLE ONLY public.receta DROP CONSTRAINT receta_pkey;
       public            postgres    false    203            -           2606    16457    users users_pkey 
   CONSTRAINT     N   ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);
 :   ALTER TABLE ONLY public.users DROP CONSTRAINT users_pkey;
       public            postgres    false    201            /           2606    16459    users users_username_key 
   CONSTRAINT     W   ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_username_key UNIQUE (username);
 B   ALTER TABLE ONLY public.users DROP CONSTRAINT users_username_key;
       public            postgres    false    201            2           2606    16471    receta fk_users    FK CONSTRAINT     u   ALTER TABLE ONLY public.receta
    ADD CONSTRAINT fk_users FOREIGN KEY (username) REFERENCES public.users(username);
 9   ALTER TABLE ONLY public.receta DROP CONSTRAINT fk_users;
       public          postgres    false    2863    203    201            �   �
  x�uY˒ۺ]�_��]%+3O�sw��'eW&׉�R�Db$L@�Iٚ}� ?p�^�ŭ��J���$_�s H͌�)�F��ӧ��y�;��|�)M����cgK�*��0���X�o�Q���_nu�Kߨֻ���|�����o���&�U��F�)��ǲ�9�Pj;Y���-�)�F��x�a�ӳ5���+X�?�0��w^=-��$0U�j[�Jw�Jo���T{�ֽi�+���V[_���Kc�R�J�y�mJZtSy%;״��F'aveK����X�0����tZ�ʻ2����n�;�Tuy���Q�wf�{��a;�p��x	So��쇀�}��
;��^��I���P�����%«��6�mX�6}��P�����z�i�Ѫ3�_�������W��'�Uq���o��7�1��֗�i�F+˭m�b�:13���o�tS��������°�����:W�P"$���4�iJ+�;m���j�K�#S�ȍ�FH{ϟ���?��ԥ�J����$�Jy zt��jDn��V�/Ͼ}�Z�k��&���u�b.���i.�NWb�%S�r��z-0�e���u����6ӥ`�����!�p�~;�^:`��F�͆C�d��g�.LI�8r������A̤-�k`np��������<t��;��C)nL�cl"&���?�Hhs��6�V�wK�6y���W�e�?K��b,ucC-y��G~�E�M7��H��-5����	*�1�uH'�U�_����J��:Z����nq*/�2��~�ō���4��t�2%^�����8Ӭ����y�TY�dA�kM��@�י���*af�"X�����2��dv�$&p x\>����w: �uL��|6�p3 G�;0Z��� ]$�t����	4��	PO��hf�A���&*�{�+��al˺x���d7�@҆@ܙ��LJ�h/B��<F��=O�(�.B0�9�
�"�ѭ�b��f���o�^.�<n�7-p1:.��Dc-d�dW���"�˦d����@y�PN�(��[T?��tHN���id㘴�S,S�D,w�R�c���"|��b��?#F�1�}I�JY�Ha���%�����To��x@wo=,�s�=k.�=@�0�X4j�B�9&8�����d1|1
�y)|U�:p1�Y���JF?R�� �W�=4�=�V�SP�y�q�1@��F�	��)��P��'�įb���&H;� &�X��܃��٧��IۤH/��f���4�Q�gc��k�z����v �c����TW1�"��b*-��"1.o|�ųnp�V]���/UYG��h`����%��TY�&�f��RG&��t��%G����夥�=�ב�sȪr����D{�X{ֳG���L�#����$-kϧ��n��yv�Qrp�n�l,إ�AR"�T#]\�4'KZ��R��XA Em`�J��d��P��;�d䴖jp�s���
� MwQ'2r�Y�vT�5�P�(��v�<��1Q��jp�)��_7sy�b�]ο�8n�`�@X-���.�XBn�0���/r��H����Y�����Rm|�������S���.�Lhl�)�����izڧ��'��%���G�'1����Y���k9�HUfOY�HQ�" ��{Q֮��NX`~լ���,J�&}�VM3_��62���L��9�l{h��gH�y��]����^���kAT�$/����<Fr����V��,sS0�$�)7��{Hm$�TN!U�Xͺ���dR���B}w��b���`ot�=�H�~���f~�5U��l>X*��p�����#�`M�q��,���,Z��EQ�n +i�(?"c$E-���"���K��KY� �)M�6�	Gb����ɥ�#����&4U�SR�L�B�OX�
(]��v�f������X#9no�]�Z�	a�Cr N���[����˨k��et%ԑ���x3� g��t��6���dP{d�vQY���Y9�C��P�F���Q��Ac���ab��ZF�.�Q�����蘍F�0��
B#��46��\�b_��/��s�5�ۯ�u���l����'z��I|Il�s4E:�݉2c��#F*Ů��hJ��GnF=:�ĸ��HD�@ܮ�I����"֟t��~��&���Qlg�=B�`󫧙^�OgŹO.N�����֚���H���tŒv>q�_m��r(H��Bb���ؖ���E��e@+��m��n���6DN�.ҘAn����FP�ߡ��_����0�蝘Nhc�����ǋ���΄��Е:�bFM���Ïp�	�������{"�F����`�{���A�/Ew(z6*z.����MRI��fKb{l߂��x����W�U��lƠ�x�6���Zf"l� �}ʍ���cJU�����|ë��D٭;�v��Y�gN!�۔Ե6�X٦�4���#!y��dq�O��g|53���n��G*��O���Ҍ�>|i�s�J����l�����H=��T;odZ�6 u�@�Z��||5��X��D���T#nd��K�z.]y,N��O���J��]>ڠ�d�B0x�9���T&b�4���ͨj�p�2�6ab?*��Rޯg��L���K�x��9q�����S� u���'�?	08{�x�j$()�&�X�@�!@T��:��r��܊�G\=�x 
i�ڈԫҕldo������ɓ'��&0      �   )  x����n�0��ݖ0�� ����q�	f�	<}���j�[�J�|6�?��A�<`�@�(��Uӏ�VDŝ�q��B̉D6��u��������i'��KG��"au����F)�I�z$�G��V�h`���;f毴>�v�О���l��<w�Q:�����ިC�؜3��Jv���(Bigw��kﲒ�qB�z����D�����r��>�$���щ\	0X��ߔe��N�`��_O(�5J�����C@�_�r�x�={�xab!��D�����i;�����`�����3H5Χ��,� �g�w%�nG��T��1m��v��sM���8R&��.��>#�,M�����9��Kz �E��X�QԮ��5v���=)5T1�tIr�z���J	�m������\��Eb�#X�MQ��ny Tv��e|�xr^JUE?��p}�q���Z�uh����8��%�H���uO��}�a���5�Q6��ISWvv06��2:�ٖ)���\'�v�ʅ(�L� ����@�ID�*��o� ����0���b6��Ev     
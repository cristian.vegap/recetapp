const router = require("express").Router();
const userModel = require("../models/user");
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");

// Se registra el cliente dentro de la BD
router.post("/register", async (req, res) => {
  try {
    const userValid = await userModel.findOne({
      where: {
        username: req.body.username,
      },
    });
    console.log(req.body);
    if (userValid) return res.status(400).send("este usuario ya existe");
    //encriptacion de contraseña
    const salt = await bcrypt.genSalt(10);
    const hashPass = await bcrypt.hash(req.body.pass, salt);

    // se añade el usuario a la lista de usuarios
    const user = await userModel.create({
      username: req.body.username,
      email: req.body.email,
      contraseña: hashPass,
    });
    return res.send(user);
  } catch (error) {
    return res.status(400).send(error);
  }
});

//Se entrega el token y el usuario de la bd que se loguea.
router.post("/login", async (req, res) => {
  try {
    const usuario = await userModel.findOne({
      where: {
        username: req.body.username,
      },
    });
    console.log(req.body);
    if (!usuario) return res.status(400).send("este usuario no existe");
    const validPass = bcrypt.compare(req.body.pass, usuario.contraseña);
    if (!validPass) return res.status(400).send("contraseña incorrecta");
    const token = jwt.sign({ id: usuario.id }, process.env.SECRET_TOKEN);
    return res.header("token", token).send(usuario);
  } catch (error) {
    return res.status(400).send(error);
  }
});
module.exports = router;

const express = require("express");
const router = express.Router();
const bd = require("../config/database");
const recetaModel = require("../models/receta");

router.use(function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "*"); // update to match the domain you will make the request from
  res.header(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept"
  );
  next();
});

// Retorna lista de recetas
router.get("/", (req, res) =>
  // FIND FROM TABLE
  recetaModel
    .findAll()
    .then((recetas) => {
      res.send(recetas);
    })
    .catch((err) => console.log(err))
);
// Retorna la receta con cierta id
router.get("/:id", async (req, resp) => {
  try {
    await recetaModel
      .findAll({
        where: {
          id: req.params.id,
        },
      })
      .then((receta) => {
        resp.send(receta);
      });
  } catch (error) {
    console.log(error);
    resp.status(400).send(error);
  }
});
// Retorna las recetas con cierta id de usuario
router.get("/user/:id", async (req, resp) => {
  try {
    await recetaModel
      .findAll({
        where: {
          idusuario: req.params.id,
        },
      })
      .then((recetas) => {
        resp.send(recetas);
      });
  } catch (error) {
    console.log(error);
    resp.status(400).send(error);
  }
});

// Agrega una receta a la lista de recetas
router.post("/add", async (req, res) => {
  try {
    const genReceta = await recetaModel.create(req.body);
    res.send(genReceta);
  } catch (error) {
    res.status(400).send(error);
  }
});

// Eliminar una receta de la lista de recetas
router.delete("/delete/:id", async (req, res) => {
  try {
    await recetaModel
      .destroy({
        where: {
          id: req.params.id,
        },
      })
      .then(() => res.send("success"));
  } catch (error) {
    res.status(400).send(error);
  }
});

module.exports = router;

const jwt = require("jsonwebtoken");

// se confirma si tiene autorización con el token recibido
function verifySign(req, res, nxt) {
  const token = req.header("token");
  if (!token) return res.status(401).send("no tienes autorizacion");
  try {
    const payload = jwt.verify(token, process.env.SECRET_TOKEN);
    req.email = payload;
    nxt();
  } catch (error) {
    return res.status(401).send("no tienes autorizacion");
  }
}

module.exports = verifySign;

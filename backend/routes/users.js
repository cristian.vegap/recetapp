const express = require("express");
const router = express.Router();
const bd = require("../config/database");
const userModel = require("../models/user");

router.use(function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "*"); // update to match the domain you will make the request from
  res.header(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept"
  );
  next();
});

// Retorna lista de usuarios
router.get("/", (req, res) =>
  // FIND FROM TABLE
  userModel
    .findAll()
    .then((users) => {
      res.send(users);
    })
    .catch((err) => console.log(err))
);
// Retorna el usuario con cierta id
router.get("/:id", async (req, resp) => {
  try {
    await userModel
      .findAll({
        where: {
          id: req.params.id,
        },
      })
      .then((user) => {
        resp.send(user);
      });
  } catch (error) {
    console.log(error);
    resp.status(400).send(error);
  }
});

// Eliminar un usuario de la lista de usuarios
router.delete("/delete/:id", async (req, res) => {
  try {
    await userModel
      .destroy({
        where: {
          id: req.params.id,
        },
      })
      .then(() => res.send("succes"));
  } catch (error) {
    res.status(400).send(error);
  }
});

module.exports = router;

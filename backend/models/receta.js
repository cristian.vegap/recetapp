const Sequelize = require("sequelize");
const bd = require("../config/database");

// MODELO DE RECETA

const receta = bd.define("receta", {
  username: {
    type: Sequelize.STRING,
  },
  titulo: {
    type: Sequelize.STRING,
  },
  ingredientes: {
    type: Sequelize.STRING,
  },
  preparacion: {
    type: Sequelize.STRING,
  },
});

module.exports = receta;

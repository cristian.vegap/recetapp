const Sequelize = require("sequelize");
const bd = require("../config/database");

// MODELO DE USER

const user = bd.define("user", {
  username: {
    type: Sequelize.INTEGER,
  },
  contraseña: {
    type: Sequelize.STRING,
  },
  email: {
    type: Sequelize.STRING,
  },
});

module.exports = user;

const express = require("express");
require("dotenv").config();
const path = require("path");

const sequelize = require("./config/database");

const app = express();

app.use(express.json());

//Las rutas
app.use("/users", require("./routes/users"));
app.use("/recetas", require("./routes/recetas"));
app.use("/auth", require("./routes/authRoutes"));

const PORT = process.env.PORT || 3001;
app.listen(PORT, console.log(`Server started on port ${PORT}`));
try {
  sequelize.authenticate();
  console.log("Connection has been established successfully.");
} catch (error) {
  console.error("Unable to connect to the database:", error);
}

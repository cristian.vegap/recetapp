import { ACTION_LOGIN, ACTION_LOGOUT } from '../actions/authActions.js';
import AsyncStorage from '@react-native-async-storage/async-storage';

const initialState = {
    isLogged: AsyncStorage.getItem('token') ? true : false,
};

const authReducer = (state = initialState, action) => {
    switch (action.type) {

        case ACTION_LOGIN:
            return {
                ...state,
                ...action.payload,
            };
        case ACTION_LOGOUT:
            return {
                ...state,
                ...action.payload,
            };
        default:
            return state;
    }
}

export default authReducer;
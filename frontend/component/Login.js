import * as React from "react";
import { View, Text, TextInput, Pressable } from "react-native";

import styles from "../assets/Styles/LightMode";
import RedStyle from "../assets/Styles/MainRed";

import axios from "axios";
import { useDispatch } from 'react-redux';

import AsyncStorage from '@react-native-async-storage/async-storage';

const Separator = () => <View style={styles.separator} />;
const SmallSeparator = () => <View style={styles.smallSeparator} />;
const BigSeparator = () => <View style={styles.bigSeparator} />;

const Login = ({ navigation }) => {

  //variables a utilizar para el inicio de sesion
  const [username, setUsername] = React.useState("");
  const [password, setPassword] = React.useState("");
  const dispatch = useDispatch();

  //funcion asincrona para el inicio de sesion

  const handleSubmit = async () => {
    axios.post('http://192.168.1.94:3001/auth/login', {
      username: username,
      pass: password,
    }).then(async (data) => {

      await AsyncStorage.setItem('@recetApps:username', data.data.username);
      await AsyncStorage.setItem('@recetApps:token', data.headers.token);
      await AsyncStorage.setItem('@recetApps:email', data.data.email);

      navigation.navigate("Home");

    }).catch((error) => {
      alert("Error al iniciar sesión")
    });
  }

  return (
    <View style={styles.container}>
      {
        //Se crea una pagina para iniciar sesion con los textinput correspondientes
      }
      <Text style={RedStyle.title}>Iniciar Sesión</Text>
      <BigSeparator />
      <Text style={RedStyle.text}>Usuario</Text>
      <SmallSeparator />
      <TextInput
        placeholderTextColor="#888888"
        placeholder="Usuario"
        value={username}
        onChangeText={setUsername}
        style={RedStyle.textInput}
      />
      <Separator />
      <Text style={RedStyle.text}>Contraseña</Text>
      <SmallSeparator />
      <TextInput
        secureTextEntry={true}
        placeholderTextColor="#888888"
        placeholder="Contraseña"
        value={password}
        onChangeText={setPassword}
        style={RedStyle.textInput}
      />
      <BigSeparator />
      <BigSeparator />
      <View
        style={{ flex: 2, justifyContent: "flex-end", alignItems: "center" }}
      >
        <Text fontSize="40" lineHeight="50">
          ¿No tienes cuenta?
          <Text
            style={RedStyle.link}
            onPress={() => navigation.navigate("Register", { id: 12 })}
          >
            {" "}
            Registrate
          </Text>
        </Text>
      </View>
      <View
        style={{ flex: 3, justifyContent: "flex-end", alignItems: "center" }}
      >
        <Pressable
          justifyContent="center"
          alignItems="center"
          style={RedStyle.button}
          onPress={handleSubmit}
        >
          <Text style={RedStyle.textButton}>Iniciar Sesion</Text>
        </Pressable>

        <BigSeparator />
      </View>
    </View>
  );
};

export default Login;

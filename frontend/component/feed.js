import * as React from "react";
import { useScrollToTop } from "@react-navigation/native";
import axios from "axios";
import { Text, View, RefreshControl } from "react-native";
import styles from "../assets/Styles/LightMode";
import Card from "../assets/Styles/Card";
import { ScrollView } from 'react-native';

const SmallSeparator = () => <View style={styles.smallSeparator} />;
const BigSeparator = () => <View style={styles.bigSeparator} />;
const GigaSeparator = () => <View style={styles.gigaSeparator} />;


{
    //Se crea una plantilla con la informacion de las recetas
}
const Feed = ({ navigation }) => {

    const [content, setContent] = React.useState()
    const [refreshing, setRefreshing] = React.useState(false)

    {
        //Funcion auxiliar para crear un wait para la funcion onRefresh
    }
    function wait(timeout) {
        return new Promise(resolve => {
            setTimeout(resolve, timeout);
        });
    }

    {
        //Funcion para refrescar la base de datos
    }
    const onRefresh = React.useCallback(() => {
        setRefreshing(true)
        wait(20).then(() => {
            setRefreshing(false)
            getFeed();
        })
    }, [refreshing])
    const ref = React.useRef(null);
    useScrollToTop(ref);

    {
        //Se funcion para obtener las recetas de la base de datos
    }
    const [feed, setFeed] = React.useState([]);
    getFeed = async () => {
        await axios.get('http://192.168.1.94:3001/recetas')
            .then(
                res => {
                    setFeed(res.data)
                }
            );
    }
    React.useEffect(() => {
        getFeed();
    }, [])
    return (
        <View >
            {
                //ScrollView para poner deslizar con el dedo por la feed
            }
            <ScrollView ref={ref}
                refreshControl={<RefreshControl refreshing={refreshing} onRefresh={onRefresh} />}>
                <View style={styles.container}>
                    <View style={Card.container}>
                        {
                            //Funcion map donde se obtienen los array de las recetas
                            feed.map(post =>
                                <View key={post.id}>
                                    <View style={Card.cardViewStyle}>
                                        {
                                            //Plantilla donde se imprimen los datos relevantes de la tabla receta
                                        }
                                        <Text style={Card.title}>
                                            {post.titulo}
                                        </Text>
                                        <SmallSeparator />
                                        <Text style={Card.subtitle}>
                                            Ingredientes 📝:
                                        </Text>
                                        <Text>{post.ingredientes}</Text>
                                        <SmallSeparator />
                                        <Text style={Card.subtitle}>
                                            Preparación👨‍🍳:
                                        </Text>
                                        <Text>{post.preparacion}</Text>
                                        <SmallSeparator />
                                        <Text style={styles.italic}
                                        >
                                            {"@" + post.username}
                                        </Text>
                                    </View>
                                    <BigSeparator />
                                </View>

                            )
                        }
                    </View>
                </View>
                <GigaSeparator />

            </ScrollView >
        </View>
    );
};

export default Feed;
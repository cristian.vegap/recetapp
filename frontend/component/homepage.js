import * as React from "react";
import { Text, View, Pressable } from "react-native";

import styles from "../assets/Styles/LightMode";
import RedStyle from "../assets/Styles/MainRed";
import AsyncStorage from '@react-native-async-storage/async-storage';
import Feed from "./feed";

const BigSeparator = () => <View style={styles.bigSeparator} />;

const Home = ({ navigation }) => {
  //funcion para obtener el nombre de usuario
  const [username, setUsername] = React.useState("");
  const getData = async () => {
    try {
      const value = await AsyncStorage.getItem("@recetApps:username")
      if (value != null) {
        console.log(value)
        setUsername(value)
      }
      if (value == null) {
        console.log("vacio")
      }
    } catch (error) {
      console.log(error)
    }
  }

  return (
    <View backgroundColor='#CC4125'>
      <View alignItems="center">
        <BigSeparator />

        {
          //Boton superior para agregar post
        }
        <Pressable
          justifyContent="center"
          alignItems="center"
          style={RedStyle.button}
          onPress={() => navigation.navigate("POST")}>
          <Text textAlign="center"
            style={RedStyle.textButton}>AGREGAR POST
          </Text>
        </Pressable>
      </View>
      {
        //Se agregan las recetas a la pagina homepage
      }
      <Feed />
    </View >
  );
};

export default Home;

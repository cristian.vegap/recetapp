import * as React from "react";
import AsyncStorage from '@react-native-async-storage/async-storage';
import axios from "axios";

import styles from "../assets/Styles/LightMode";
import RedStyle from "../assets/Styles/MainRed";
import { View, Text, TextInput, Pressable } from "react-native";

const Separator = () => <View style={styles.separator} />;
const SmallSeparator = () => <View style={styles.smallSeparator} />;
const BigSeparator = () => <View style={styles.bigSeparator} />;

const Post = ({ navigation }) => {

    //variables necesarias para la creacion de un post
    const [titulo, setTitulo] = React.useState("")
    const [ingredientes, setIngredientes] = React.useState("")
    const [preparacion, setPreparacion] = React.useState("")
    const [username, setUsername] = React.useState("")

    //funcion que agrega un post a la base de datos
    const handleSubmit = () => {
        axios.post('http://192.168.1.94:3001/recetas/add', {
            username: username,
            titulo: titulo,
            ingredientes: ingredientes,
            preparacion: preparacion,
        }).then((data) => {
            console.log(data);
            navigation.navigate("Home");
        }).catch((error) => {
            console.log(error);
        });
    }

    //funcion para obtener el nombre del ususario que publico el post
    const [loaded, setLoaded] = React.useState(false);
    React.useEffect(() => {
        const fetchData = async () => {
            if (!loaded) {
                const storage = await AsyncStorage.getItem('@recetApps:username');
                setUsername(storage);
                console.log("nombre: " + storage);
                if (storage != "") {
                    setLoaded(true);
                }
            }
        }
        fetchData()
    });

    return (
        <View style={styles.container}>
            {
                //Plantilla que obtiene los datos por pantalla y setea los valores obtenidos en las distintas variables
            }
            <Text style={RedStyle.title}>Nueva Receta!</Text>
            <BigSeparator />
            <Text style={RedStyle.text}>Titulo</Text>
            <SmallSeparator />
            <TextInput
                placeholderTextColor="#888888"
                placeholder="Titulo"
                value={titulo}
                onChangeText={setTitulo}
                style={RedStyle.textInput}
            />
            <Separator />
            <Text style={RedStyle.text}>Ingredientes</Text>
            <SmallSeparator />
            <TextInput
                placeholderTextColor="#888888"
                placeholder="Ingredientes"
                value={ingredientes}
                onChangeText={setIngredientes}
                style={RedStyle.textInput}
            />
            <Separator />
            <Text style={RedStyle.text}>Preparacion</Text>
            <SmallSeparator />
            <TextInput
                placeholderTextColor="#888888"
                placeholder="Preparacion"
                value={preparacion}
                onChangeText={setPreparacion}
                style={RedStyle.textInput}
            />
            <BigSeparator />
            <BigSeparator />
            <View style={{ flex: 3, justifyContent: "flex-end", alignItems: "center" }}>
                <Pressable
                    justifyContent="center"
                    alignItems="center"
                    style={RedStyle.button}
                    onPress={handleSubmit}

                >
                    <Text style={RedStyle.textButton}>Subir receta</Text>
                </Pressable>
                <BigSeparator />
            </View>
        </View>
    );
};

export default Post;
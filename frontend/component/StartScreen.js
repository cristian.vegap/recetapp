import * as React from "react";
import {
  View,
  Text,
  StyleSheet,
  Pressable,
  Image,
  ImageBackground,
} from "react-native";

const Separator = () => <View style={styles.separator} />;

const bg = require("../assets/bg-ipad.png");
const logo = require("../assets/logoWhite.png");

const StartScreen = ({ navigation }) => {
  return (
    <View style={styles.container}>
      <ImageBackground source={bg} style={styles.bg}>
        <Image source={logo} style={styles.image} />

        {/*--------------Registrar-----------------*/}
        <Pressable
          style={styles.button}
          onPress={() => navigation.navigate("Register", { id: 12 })}
        >
          <Text style={styles.text}>Registrar</Text>
        </Pressable>
        <Separator />
        {/*--------------Login-----------------*/}
        <Pressable
          style={styles.button}
          onPress={() => navigation.navigate("Login")}
        >
          <Text style={styles.text}>Iniciar Sesion</Text>
        </Pressable>
      </ImageBackground>
    </View>
  );
};

//Estilos
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#CC4125",
  },
  separator: {
    marginVertical: 8,
  },
  image: {
    width: "80%",
    height: "70%",
    resizeMode: "contain",
  },
  button: {
    width: "70%",
    height: 50,
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 50,
    elevation: 3,
    backgroundColor: "white",
  },
  text: {
    fontSize: 16,
    lineHeight: 21,
    fontWeight: "bold",
    letterSpacing: 0.25,
    color: "#CC4125",
  },
  bg: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
  },
});

export default StartScreen;

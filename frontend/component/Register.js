import * as React from "react";
import { useState, useEffect } from "react";
import { View, Text, TextInput, Pressable } from "react-native";
import axios from "axios";

import styles from "../assets/Styles/LightMode";
import RedStyle from "../assets/Styles/MainRed";

const Separator = () => <View style={styles.separator} />;
const SmallSeparator = () => <View style={styles.smallSeparator} />;
const BigSeparator = () => <View style={styles.bigSeparator} />;



const Register = ({ navigation, route }) => {
  const id = route.params.id;

  //variables necesarias para crear un usuario
  const [username, setUsername] = React.useState("");
  const [password, setPassword] = React.useState("");
  const [email, setEmail] = React.useState("");

  const [recetas, setRecetas] = React.useState([]);
  const [loaded, setDataLoaded] = useState(false);

  //Se mandan los datos obtenidos por la aplicacion a la base de datos
  const handleSubmit = (e) => {
    e.preventDefault();
    console.log("Registrando:")
    console.log(username);
    axios.post('http://192.168.1.94:3001/auth/register', {
      email: email,
      pass: password,
      username: username,
    }).then((data) => {
      navigation.navigate("Login");
      console.log(data);
    }).catch((error) => {
      alert("Error al crear la cuenta")
    });
  }

  return (
    <View style={styles.container}>
      <Text style={RedStyle.title}>Registrate</Text>
      <BigSeparator />
      {/* CORREO---------------------------------*/}
      <Text style={RedStyle.text}>Correo</Text>
      <SmallSeparator />
      <TextInput
        placeholderTextColor="#888888"
        placeholder="Ejemplo@correo.com"
        value={email}
        onChangeText={setEmail}
        style={RedStyle.textInput}
      />
      <Separator />
      {/* USUARIO---------------------------------*/}
      <Text style={RedStyle.text}>Usuario</Text>
      <SmallSeparator />
      <TextInput
        placeholderTextColor="#888888"
        placeholder="Usuario"
        value={username}
        onChangeText={setUsername}
        style={RedStyle.textInput}
      />
      <Separator />
      {/* CONTRASEÑA----------------------------*/}
      <Text style={RedStyle.text}>Contraseña</Text>
      <SmallSeparator />
      <TextInput
        secureTextEntry={true}
        placeholderTextColor="#888888"
        placeholder="Contraseña"
        value={password}
        onChangeText={setPassword}
        style={RedStyle.textInput}
      />

      {/* TEXTO PEQUEÑO Y BOTON FINAL*/}
      <View
        style={{ flex: 2, justifyContent: "flex-end", alignItems: "center" }}
      >
        <Text fontSize="40" lineHeight="50">
          ¿Ya tienes cuenta?
          <Text
            style={RedStyle.link}
            onPress={() => navigation.navigate("Login")}
          > Inicia Sesion
          </Text>
        </Text>
      </View>
      <View
        style={{ flex: 3, justifyContent: "flex-end", alignItems: "center" }}
      >
        <Pressable
          justifyContent="center"
          alignItems="center"
          style={RedStyle.button}
          onPress={handleSubmit}
        >
          <Text style={RedStyle.textButton}>Registrarse</Text>
        </Pressable>
        <BigSeparator />
      </View>
    </View>
  );
};

export default Register;

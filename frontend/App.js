// In App.js in a new project

import * as React from "react";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";

import Register from "./component/Register.js";
import StartScreen from "./component/StartScreen.js";
import Login from "./component/Login.js";
import Home from "./component/homepage.js";
import Post from "./component/post";
import Feed from "./component/feed";

import { Provider } from 'react-redux'
import store from "./redux/store.js";

const Stack = createStackNavigator();


function App() {
  return (
    <Provider store={store}>
      <NavigationContainer>
        {// Se crean las rutas para las distintas paginas
        }
        <Stack.Navigator initialRouteName={"Start"}>
          <Stack.Screen
            name="Volver"
            options={{ headerShown: false }}
            component={StartScreen}
          />
          <Stack.Screen
            name="Register"
            options={{ title: "Registrar" }}
            component={Register}
          />
          <Stack.Screen
            name="Login"
            options={{ title: "Iniciar Sesion" }}
            component={Login}
          />
          <Stack.Screen
            name="Home"
            options={{ headerShown: false }}
            component={Home}
          />
          <Stack.Screen
            name="POST"
            options={{ title: "POST" }}
            component={Post}
          />
          <Stack.Screen
            name="Feed"
            options={{ title: "Feed" }}
            component={Feed}
          />
        </Stack.Navigator>
      </NavigationContainer>
    </Provider>

  );
}

export default App;

import * as React from 'react';
import { StyleSheet } from 'react-native';

//Estilo para botones blancos
const whiteButtonStyles = StyleSheet.create({
    button: {
        width: "70%",
        height: 50,
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 50,
        elevation: 3,
        backgroundColor: 'white',
    },
    text: {
        fontSize: 16,
        lineHeight: 21,
        fontWeight: 'bold',
        letterSpacing: 0.25,
        color: '#CC4125',
    },
});

export default whiteButtonStyles;



import * as React from 'react';
import { StyleSheet } from 'react-native';

//Estilo principal
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        padding: 30,
    },
    separator: {
        marginVertical: 10,
    },
    smallSeparator: {
        marginVertical: 6,
    },
    bigSeparator: {
        marginVertical: 20,
    },
    gigaSeparator: {
        marginVertical: 80,
    },

    bold: { fontWeight: 'bold' },
    italic: {
        fontStyle: 'italic',
        textAlign: "right",
    },
    underline: { textDecorationLine: 'underline' }


});

export default styles;
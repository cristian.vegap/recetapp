import * as React from 'react';
import { StyleSheet } from 'react-native';

//Estilo que asemeja a tipo card para las recetas
const Card = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        paddingVertical: 20,
    },
    cardViewStyle: {
        backgroundColor: '#F7F7F7',
        borderRadius: 10,
        padding: 15,
        shadowOffset: {
            width: 0,
            height: 3
        },
        shadowRadius: 3,
        shadowOpacity: 0.5
    },
    title: {
        fontSize: 20,
        fontWeight: 'bold',
        letterSpacing: 0.25,
        color: '#CC4125',
    },
    subtitle: {
        fontSize: 15,
        fontWeight: 'bold',
        letterSpacing: 0.25,
    }


});

export default Card;
import { StyleSheet } from 'react-native';

//Estilo para las distintas componentes rojas
const RedStyle = StyleSheet.create({
    button: {
        width: "70%",
        height: 50,
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 50,
        elevation: 3,
        backgroundColor: '#CC4125',
    },
    title: {
        fontSize: 40,
        lineHeight: 50,
        fontWeight: 'bold',
        letterSpacing: 0.25,
        color: '#CC4125',
        textAlign: 'left',
    },
    text: {
        fontSize: 20,
        lineHeight: 20,
        fontWeight: '500',
        letterSpacing: 0.25,
        color: 'black',
        textAlign: 'left',
    },
    textInput: {
        width: "100%",
        height: 35,
        paddingHorizontal: 30,
        borderColor: '#C8C8C8',
        borderWidth: 2,
        borderRadius: 60,
        backgroundColor: '#F7F7F7'
    },
    textButton: {
        fontSize: 16,
        lineHeight: 21,
        fontWeight: 'bold',
        letterSpacing: 0.25,
        color: 'white',
    },
    link: {
        color: '#CC4125',
    }
});

export default RedStyle;